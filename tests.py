import shelf_splitter1
import sys
import numpy as np


def assert_equal(a,b):
    tmp = str(type(a))
    if (isinstance(a, list) or 'ndarray' in str(type(a))):
        if (a!=b).any():
            assert False,(str(a) + " is not equal to " + str(b))
    elif (a!=b):
        assert False,(str(a) + " is not equal to " + str(b))

class Image_mock:

    def __init__(self,pixels):
        self.pixels = pixels
        self.size = (len(pixels),2)
        return

    def getpixel(self,dim):
        return self.pixels[dim[0]]

def all_tests():
    test_calc_break_lines()
    test_find_candidate_lines()




def test_find_candidate_lines():
    image = Image_mock([(0,0,0)])

    s1 = shelf_splitter1.Section((0,0,100,1), image)
    s1.line_breaks = [100,150,200]
    s2 = shelf_splitter1.Section((0,5,100,6), image)
    s2.line_breaks = [50,150,250]
    res = s1.find_candidate_lines(s2)
    assert res == [(150,0,150,5)]


    s1.line_breaks = [100,140,151,160,200]
    s2.line_breaks = [50,149,250]
    res = s1.find_candidate_lines(s2)
    assert res == [(151,0,149,5)]


    s1.line_breaks = [100,150]
    s2.line_breaks = [100,300,350]
    res = s1.find_candidate_lines(s2)
    assert res == [(100,0,100,5)]


    s1.line_breaks = [100,150,200]
    s2.line_breaks = [0,10,20,30,200]
    res = s1.find_candidate_lines(s2)
    assert res == [(200,0,200,5)]












def test_calc_break_lines():
    image = Image_mock([
        (50, 50, 50),(50,50,50),(50,50,50),(50,50,50),(50,50,50),(50,50,50),(50,50,50),(50,50,50),(50,50,50),(50,50,50),
        (50,50,50),(50,50,50),(50,50,50),(50,50,50),(50,50,50),(50,50,50),(50,50,50),(50,50,50),(50,50,50),
        (0,0,0)])
    s = shelf_splitter1.Section((),image)
    s.calc_break_lines()
    assert []==s.line_breaks

    image = Image_mock([
        (50, 50, 50),(50,50,50),(50,50,50),(50,50,50),(50,50,50),(50,50,50),(50,50,50),(50,50,50),(50,50,50),(50,50,50),
        (50, 50, 50),(50,50,50),(50,50,50),(50,50,50),(50,50,50),(50,50,50),(50,50,50),(50,50,50),(50,50,50),(50,50,50),
        (0,0,0)])
    s = shelf_splitter1.Section((),image)
    s.calc_break_lines()
    assert [20]==s.line_breaks


    return

if __name__ == "__main__":
    all_tests()