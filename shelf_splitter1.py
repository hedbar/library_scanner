import math
from PIL import Image
import matplotlib.pyplot as plt
import numpy as np

SECTION_DIFF = 50
SECTION_WIDTH = 5


class Section:
    def __init__(self, pos,image):
        self.image = image
        self.w, self.h = image.size
        self.pos = pos
        self.line_breaks = []

    def calc_break_lines(self):
        BLACK_THREASHOLD = 4000
        STRETCH_THREASHOLD = 20
        is_previous_black = is_current_black = False
        stretch = 0

        black_ditances = []

        for i in range(0,self.w):
            r,g,b = self.image.getpixel((i,0))
            black_distance = (r*r + g*g + b*b)
            # black_ditances.append(black_distance)
            is_current_black =  black_distance < BLACK_THREASHOLD
            if (is_previous_black == is_current_black):
                stretch += 1
            else: # A Change
                if (stretch >= STRETCH_THREASHOLD and is_current_black):
                    self.line_breaks.append(i)
                stretch = 0
        # print(black_ditances)

        # plt.plot(np.digitize(black_dist,[4000]))
        # plt.show()
        return

    def find_candidate_lines(self,other):
        slope_threashold = math.floor(abs((self.pos[1] - other.pos[1]))/2)
        ret = []
        other_index = 0
        for b in self.line_breaks:
            while (other_index<len(other.line_breaks) and other.line_breaks[other_index] < b + slope_threashold):
                other_index +=1
            if (other_index!=0 and other.line_breaks[other_index-1] >=  b - slope_threashold):
                ret.append(  (b,self.pos[1],other.line_breaks[other_index-1],other.pos[1]) )
        return ret

def main():
    im = Image.open("./data/input/shelf1.jpg")
    w,h = im.size
    w = w-1; h=h-1
    # print(w,h)

    sections = []
    short_line_candidates = None
    number_of_sections = int(h / SECTION_DIFF)

    # if (True):
    #     i = 1
    for i in range(1,number_of_sections):

        for j in range(0,2):
            lower = (i+1) * SECTION_DIFF + j*SECTION_WIDTH
            upper = lower + 1 + j*SECTION_WIDTH
            t = (0,lower,w,upper)
            s = Section(t,im.crop(t))
            sections.append(s)
            s.calc_break_lines()
            if (j!=0 and short_line_candidates != None):
                tmp_long_line_candidates = s.find_candidate_lines(short_line_candidates) -- TODO

        short_line_candidates = sections[len(sections) - 2].find_candidate_lines(sections[len(sections)-1])



if __name__ == "__main__":
    main()
