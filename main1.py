import os
from skimage import io
from skimage import filters
from skimage import segmentation
import time
import numpy as np

class dotdict(dict):
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

def measure_time():
    tmp = time.time()
    print ("Seconds passed:" + str(tmp - measure_time.clock))
    measure_time.clock = tmp
measure_time.clock = time.time()


def add_neighbours_count(labels,x,y,obj):
    for i in range(-1,2):
        for j in range(-1,2):
            if ((i == 0 and j == 0) or x+i<0 or y+j<0 or x+i==labels.shape[0] or y+j==labels.shape[1]): continue
            if (labels[x+i,y+j] == labels[x,y]): continue
            if (labels[x+i,y+j] in obj):
                obj[labels[x+i,y+j]] += 1
            else :
                obj[labels[x+i,y+j]] = 1
    return


def generate_segs_metadata(segments_slic):
    segs = {}
    for i in range(segments_slic.shape[0]):
        for j in range(segments_slic.shape[1]):
            if (segments_slic[i, j] in segs):
                tmp = segs[segments_slic[i, j]]
                tmp.count += 1
                tmp.borders.min_x = min(tmp.borders.min_x, i)
                tmp.borders.max_x = max(tmp.borders.max_x, i)
                tmp.borders.min_y = min(tmp.borders.min_y, j)
                tmp.borders.max_y = max(tmp.borders.max_y, j)
                add_neighbours_count(segments_slic, i, j, tmp.neighbours)
            else:
                base_seg_def = dotdict({"count": 1,
                                "borders": dotdict({"max_x": i, "min_x": i, "max_y": j, "min_y": j}),
                                "neighbours": dotdict({})
                                })
                add_neighbours_count(segments_slic, i, j, base_seg_def.neighbours)
                segs[segments_slic[i, j]] = base_seg_def
    return segs


def generate_replacement_map(segs_metadata,threashold):
    map = {}
    max_neighbour=(0,0)
    for seg in segs_metadata:
        if (segs_metadata[seg].count < threashold):
            max_neighbour=(0,0)
            for n in segs_metadata[seg].neighbours:
                if (segs_metadata[seg].neighbours[n] > max_neighbour[1]):
                    max_neighbour = (n,segs_metadata[seg].neighbours[n])
            map[seg] = max_neighbour[0]
    return map

def replace_labels(segments_labeled,replacement_map):
    for i in range(segments_labeled.shape[0]):
        for j in range(segments_labeled.shape[1]):
            if (segments_labeled[i, j] in replacement_map):
                segments_labeled[i, j] = replacement_map[segments_labeled[i, j]]


if __name__ == "__main__":
    img = io.imread("C:/Users/User/Desktop/bitbucket/library_scanner/img1_small.jpg")
    #print (img.dtype)
    #print (img.shape)

    #segments_labeled = segmentation.slic(img,n_segments=100)
    segments_labeled = segmentation.quickshift(img)

    measure_time()

    segs = generate_segs_metadata(segments_labeled)

    SEG_THREASHOLD = 25000

    replacement_map = generate_replacement_map(segs,SEG_THREASHOLD)

    replace_labels(segments_labeled,replacement_map)






    print(segs)
    io.imshow(segmentation.mark_boundaries(img, segments_labeled))
    measure_time()
    io.show()