from blaze.expr.reductions import max
from skimage import io
from skimage import color
import random

from numpy import *
import csv
import sys
from skimage.transform import (hough_line, hough_line_peaks,
                               probabilistic_hough_line)
from skimage.feature import canny
from skimage import data
import numpy as np
import matplotlib.pyplot as plt
import time
import math


def measure_time(executed_chunk_id):
    tmp = time.time()
    print (executed_chunk_id + " : Seconds passed:" + str(tmp - measure_time.clock))
    measure_time.clock = tmp
measure_time.clock = time.time()




def get_alpha(line):
    p0, p1 = line
    dx = p0[0] - p1[0]
    dy = p0[1] - p1[1]
    alpha = math.pi / 2
    if (dy != 0):
        alpha = math.atan(dx/dy)
    return alpha


def get_slope(line):
    p0, p1 = line
    dx = p0[0] - p1[0]
    dy = p0[1] - p1[1]
    slope =  sys.maxsize
    if (dx != 0):
        slope = dy/dx
    return slope

def output_csv(path,lines_dict):
    try:
        of1 = open(path, 'w')
    except Exception as e:
        print(e)
        of1 = open(path + ".tmp." +str(random.randint(0,1000)), 'w')


    of1.write("x1,y1,x2,y2,dx,dy,alpha\n")

    for ind in lines_dict:
        p0, p1 = lines_dict[ind]
        of1.write(str(p0[0]) + "," + str(p0[1]) + "," +  str(p1[0]) + "," + str(p1[1]) + "," )
        dx = p0[0] - p1[0]
        dy = p0[1] - p1[1]
        of1.write(str(dx) + "," + str(dy) + "," )

        of1.write(str(get_alpha(lines_dict[ind])))

        of1.write("\n")





def extract_lowest_vertical_lines(output_file_path,slope_threashold):

    image = gray_image
    edges = canny(image)
    measure_time("canny")
    #
    # io.imshow(edges, cmap=plt.cm.gray)
    # io.show()
    #
    # exit()


    lines = probabilistic_hough_line(edges, threshold=10, line_length=50,line_gap=5) #1

    #lines = probabilistic_hough_line(edges, threshold=10, line_length=50,line_gap=5,theta = np.array([0.0]) ) #2

    # lines = probabilistic_hough_line(edges, threshold=10, line_length=50,line_gap=5,theta = np.array([math.pi*-1/10,0.0,math.pi*1/10]) ) #3

    #lines = probabilistic_hough_line(edges, threshold=10, line_length=50,line_gap=5,theta = np.arange(math.pi*-1/20, math.pi*1/20, math.pi*1/30)  ) #4

    print("Lines after hough : " + str(len(lines)) )


    measure_time("probabilistic_hough_line")



    col_width = img_width / 100;

    all_matching_lines = []
    lowest_lines = {}


    for line in lines:
        if (math.fabs(get_slope(line)) < slope_threashold ):
            continue
        all_matching_lines.append(line)
        p0, p1 = line
        lower_point = p0
        if (p1[1] > p0[1]):
            lower_point = p1
        col = math.floor(lower_point[0] / col_width)
        if (col in lowest_lines):
            q0, q1 = lowest_lines[col]
            if (lower_point[1] > q0[1] and lower_point[1] > q1[1]):
                lowest_lines[col] = line
        else:
            lowest_lines[col] = line



    output_csv(output_file_path,lowest_lines)

    #return all_matching_lines
    return lowest_lines.values()


def plot_lines(lines,title):
    fig, (ax3) = plt.subplots()
    ax3.imshow(gray_image * 0)
    for line in lines:
        p0, p1 = line
        ax3.plot((p0[0], p1[0]), (p0[1], p1[1]))

    ax3.set_title(title)
    ax3.set_axis_off()
    ax3.set_adjustable('box-forced')
    plt.show()


def process_csv(input_file_path):
    if1 = open(input_file_path, 'r')

    lines = []
    first_elem = True

    for line in csv.reader(if1):
        if (not first_elem):
            p1 = (int(line[0]),int(line[1]))
            p2 = (int(line[2]),int(line[3]))
            line = (p1,p2)
            lines.append(line)
        first_elem = False

    return lines

def log_lines(log_path,lines):
    of1 = open(log_path, 'w')
    for line in lines:
        of1.write(str(line) + "\n")


def filter_line_quartile():
    tmp = []
    for line in lines:
        p1, p2 = line
        tmp.append(np.maximum(p1[1], p2[1]))
    bounding_y1 = np.percentile(tmp, 95) + 20
    bounding_y2 = np.percentile(tmp, 50) - 20
    lines1 = []
    for line in lines:
        p1, p2 = line
        bottom = np.maximum(p1[1], p2[1])
        if (bottom > bounding_y2 and bottom < bounding_y1):
            lines1.append(line)
    log_lines("./tmp/log_lines_after vertical_bounding.txt", lines1)
    plot_lines(lines1, "only Q50 - Q95")


def sort_lines_by_lower_x_key(line1):
    return np.maximum(line1[0][0],line1[1][0])


def tuple_substruct(t1,t2) :
    return (t1[0]-t2[0],t1[1]-t2[1])


#http://www.cs.mun.ca/~rod/2500/notes/numpy-arrays/numpy-arrays.html
def perpendicular( a ) :
    b = empty_like(a)
    b[0] = -a[1]
    b[1] = a[0]
    return b

# line segment a given by endpoints a1, a2
# line segment b given by endpoints b1, b2
# return
def seg_intersect(a1,a2, b1,b2) :
    da = tuple_substruct(a2,a1)
    db = tuple_substruct(b2,b1)
    dp = tuple_substruct(a1,b1)
    dap = perpendicular(da)
    denom = dot( dap, db)
    num = dot( dap, dp )
    return (num / denom)*db + b1


def crop_image_along_lines(img,line1,line2):
    top = seg_intersect(line1[0],line1[1],(0,0),(img.shape[1],0))
    buttom = seg_intersect(line1[0],line1[1],(0,img.shape[0]),(img.shape[1],img.shape[0]))
    return (top,buttom)


if __name__ == "__main__":

    img = io.imread("C:/Users/User/Desktop/bitbucket/library_scanner/img1.jpg")
    img_width = img.shape[1]
    gray_image = color.rgb2gray(img)


    path = "./tmp/lowest_lines.csv"

    #lines = extract_lowest_vertical_lines(path,4)
    lines = process_csv(path)

    lines.sort(key=sort_lines_by_lower_x_key)

    #log_lines("./tmp/log_lines.txt",lines)



    line_0_extended = crop_image_along_lines(img,lines[0],lines[1])

    plot_lines((lines[0],lines[1]), "Test1")
    plot_lines((line_0_extended,lines[1]), "Test2")

    #filter_line_quartile()



    io.show()