import main1
import sys
import numpy as np


def assert_equal(a,b):
    tmp = str(type(a))
    if (isinstance(a, list) or 'ndarray' in str(type(a))):
        if (a!=b).any():
            assert False,(str(a) + " is not equal to " + str(b))
    elif (a!=b):
        assert False,(str(a) + " is not equal to " + str(b))

labels = np.array([1,1,1,2,2,2,3,3,3]).reshape((3,3))
print("labels = \n" + str(labels),flush=True)

result = {}
main.add_neighbours_count(labels,0,0,result)
assert_equal(result,{2:2})


main.add_neighbours_count(labels,0,1,result)
assert_equal(result,{2:5})


result = {}
main.add_neighbours_count(labels,1,1,result)
assert_equal(result,{1:3,3:3})


result = main.generate_segs_metadata(labels)
assert_equal(
        result,
        {1: {'neighbours': {2: 7}, 'count': 3, 'borders': {'max_x': 0, 'min_x': 0, 'max_y': 2, 'min_y': 0}}, 2: {'neighbours': {1: 7, 3: 7}, 'count': 3, 'borders': {'max_x': 1, 'min_x': 1, 'max_y': 2, 'min_y': 0}}, 3: {'neighbours': {2: 7}, 'count': 3, 'borders': {'max_x': 2, 'min_x': 2, 'max_y': 2, 'min_y': 0}}}
)

res = main.generate_replacement_map(result,2)
assert_equal(res,{})

print ("ALL TESTS PASSED OK",flush=True)


labels = np.array([1,1,1,2,4,2,3,3,3]).reshape((3,3))
print("labels = \n" + str(labels),flush=True)


result = main.generate_segs_metadata(labels)
assert_equal(
        result,
        {1: {'neighbours': {2: 4, 4: 3}, 'borders': {'min_x': 0, 'min_y': 0, 'max_x': 0, 'max_y': 2}, 'count': 3}, 2: {'neighbours': {1: 4, 3: 4, 4: 2}, 'borders': {'min_x': 1, 'min_y': 0, 'max_x': 1, 'max_y': 2}, 'count': 2}, 3: {'neighbours': {2: 4, 4: 3}, 'borders': {'min_x': 2, 'min_y': 0, 'max_x': 2, 'max_y': 2}, 'count': 3}, 4: {'neighbours': {1: 3, 2: 2, 3: 3}, 'borders': {'min_x': 1, 'min_y': 1, 'max_x': 1, 'max_y': 1}, 'count': 1}}
)

res = main.generate_replacement_map(result,2)
assert_equal(res,{4:1})

map = main.generate_replacement_map(result,3)
assert_equal(map,{2:1,4:1})

main.replace_labels(labels,map)
assert_equal(labels, np.array([1,1,1,1,1,1,3,3,3]).reshape((3,3)) )


print ("ALL TESTS PASSED OK",flush=True)